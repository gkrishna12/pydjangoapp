from django.contrib.auth.decorators import permission_required
from django.shortcuts import render
from django.views import generic
from .models import Book, Author, BookInstance, Genre
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
import datetime

from .forms import RenewBookForm

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

def index(request):
	"""
	View function for home page of site.
	"""
	# Number of visits to this view, as counted in the session variable.
	num_visits = request.session.get('num_visits', 0)
	request.session['num_visits'] = num_visits+1
	# Generate counts of some of the main objects
	num_books = Book.objects.all().count()
	num_instances = BookInstance.objects.all().count()
	# Available books (status = 'a')
	num_instances_available = BookInstance.objects.filter(status__exact='a').count()
	num_authors = Author.objects.count()  # The 'all()' is implied by default.

	num_genres = Genre.objects.all().count()

	num_books_fiction 	= Book.objects.filter(genre__name__contains='fiction').count()
	num_books_mythology = Book.objects.filter(genre__name__contains='mythological').count()

	# Render the HTML template index.html with the data in the context variable
	return render(
		request,
		'index.html',
		context={
			'num_books':num_books,
			'num_instances':num_instances,
			'num_instances_available':num_instances_available,
			'num_authors':num_authors,
			'num_genres':num_genres,
			'num_books_fiction':num_books_fiction,
			'num_books_mythology':num_books_mythology,
			'num_visits':num_visits
		},
	)

class BookListView(generic.ListView):
	model = Book
	template_name = 'books/book_list.html'
	paginate_by = 2

class BookDetailView(generic.DetailView):
	model = Book

	template_name = 'books/book_detail.html'

	def book_detail_view(request,pk):
		try:
			book_id = Book.objects.get(pk=pk)
		except Book.DoesNotExist:
			raise Http404("Book Does not exist")

		return render(
			request,
			'catalog/book_detail.html',
			context={'book':book_id,}
		)

class AuthorListView(generic.ListView):
	model = Author
	template_name = 'authors/author_list.html'
	paginate_by = 2

class AuthorDetailView(generic.DetailView):
	model = Author

	template_name = 'authors/author_detail.html'

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(AuthorDetailView, self).get_context_data(**kwargs)
		# Get the blog from id and add it to the context
		context['books'] = Book.objects.all()
		return context

	def author_detail_view(request,pk):
		try:
			author_id = Author.objects.get(pk=pk)
		except Author.DoesNotExist:
			raise Http404("Author Does not exist")

		return render(
			request,
			'catalog/author_detail.html',
			context={'author':author_id, }
		)

class LoanedBooksByUserListView(LoginRequiredMixin,generic.ListView):
	"""
	Generic class-based view listing books on loan to current user. 
	"""
	model = BookInstance
	template_name ='books/bookinstance_list_borrowed_user.html'
	paginate_by = 2

	def get_queryset(self):
		return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')



class LoanedBooksListView(PermissionRequiredMixin,generic.ListView):
	"""
	Generic class-based view listing books on loan to current user. 
	"""
	permission_required = 'catalog.can_mark_returned'
	model = BookInstance
	template_name ='books/bookinstance_list_borrowed.html'
	paginate_by = 2

	def get_queryset(self):
		return BookInstance.objects.filter(status__exact='o').order_by('due_back')

@permission_required('catalog.can_mark_returned')
def renew_book(request, pk):
	book_inst=get_object_or_404(BookInstance, pk = pk)

	# If this is a POST request then process the Form data
	if request.method == 'POST':

		# Create a form instance and populate it with data from the request (binding):
		form = RenewBookForm(request.POST)

		# Check if the form is valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required (here we just write it to the model due_back field)
			book_inst.due_back = form.cleaned_data['renewal_date']
			book_inst.save()

			# redirect to a new URL:
			return HttpResponseRedirect(reverse('borrowed-books') )

	# If this is a GET (or any other method) create the default form.
	else:
		proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
		form = RenewBookForm(initial={'renewal_date': proposed_renewal_date,})

	return render(request, 'books/book_renew.html', {'form': form, 'bookinst':book_inst})

class AuthorCreate(PermissionRequiredMixin,CreateView):
	permission_required = 'catalog.can_mark_returned'
	model = Author
	template_name ='authors/author_form.html'
	fields = '__all__'
	initial={'date_of_death':'12/10/2016',}

class AuthorUpdate(PermissionRequiredMixin,UpdateView):
	permission_required = 'catalog.can_mark_returned'
	model = Author
	template_name ='authors/author_form.html'
	fields = ['first_name','last_name','date_of_birth','date_of_death']

class AuthorDelete(PermissionRequiredMixin,DeleteView):
	permission_required = 'catalog.can_mark_returned'
	model = Author
	template_name ='authors/author_confirm_delete.html'
	success_url = reverse_lazy('authors')

class BookCreate(PermissionRequiredMixin,CreateView):
	permission_required = 'catalog.can_mark_returned'
	model = Book
	template_name ='books/book_form.html'
	fields = '__all__'

class BookUpdate(PermissionRequiredMixin,UpdateView):
	permission_required = 'catalog.can_mark_returned'
	model = Book
	template_name ='books/book_form.html'
	fields = '__all__'

class BookDelete(PermissionRequiredMixin,DeleteView):
	permission_required = 'catalog.can_mark_returned'
	model = Book
	template_name ='books/book_confirm_delete.html'
	success_url = reverse_lazy('books')