# README #

### What is this repository for? ###

* Venturing into my next Web Application built using python and Django. This application simulates a basic library setup that can handle Books and Authors. It can alos maintain multiple instances of a book and their respective statuses. The library can have librariance who can renew books that have crossed the due date.

### How do I get set up? ###

* To run this application you need to have python and Django installed.
* This site is currently running on heroku. Here is the [link](https://pydjangoapp.herokuapp.com/catalog/).
* Feel free to play around with the site!
	Admin username - admin, password - administrator
	Librarian username - librarian, password - libadmin01
	Member username - member, password - memdefault01

### Who do I talk to? ###

* K Gopal Krishna - mail@kgopalkrishna.com